#!/bin/bash -ex

## -- NOTE -- the Touch File bits might be removable after the next version -- NOTE the touchfile version is manually entered

# CONFIG
prefix="Aladin"
suffix=""
munki_package_name="Aladin"
display_name="Aladin"
icon_name=""
#url="https://aladin.u-strasbg.fr/java/Aladin.dmg"
url="https://aladin.cds.unistra.fr/java/Aladin.x86.dmg"



# download it (-L: follow redirects)
curl -L -o app.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' "${url}"

## EXAMPLE: unpacking a flat package to find appropriate things
## Mount disk image on temp space
mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`

## Unpack
#/usr/sbin/pkgutil --expand "${mountpoint}/*.pkg" pkg
mkdir -p build-root/Applications
app_in_dmg=$(ls -d $mountpoint/*.app)
cp -R "$app_in_dmg" build-root/Applications



# (cd build-root; pax -rz -f ../pkg/*/Payload)

# Obtain version info
version=`/usr/libexec/PlistBuddy -c "Print :CFBundleShortVersionString" "$app_in_dmg"/Contents/Info.plist`

#NOTE -- Manual Version number set:
#version=11.024

#create touch file for use with installs array'
mkdir -p build-root/Library/Managed\ Installs
touch build-root/Library/Managed\ Installs/aladin"${version}"

#identifier=`/usr/libexec/PlistBuddy -c "Print :CFBundleIdentifier" $app_in_dmg/Contents/Info.plist`

# hdiutil detach "${mountpoint}"

#creating a components plist to turn off relocation
/usr/bin/pkgbuild --analyze --root build-root/ Component-${munki_package_name}.plist

plutil -replace BundleIsRelocatable       -bool NO Component-${munki_package_name}.plist
plutil -replace BundleHasStrictIdentifier -bool NO Component-${munki_package_name}.plist

#build PKG
#/usr/bin/pkgbuild --root build-root/ --identifier edu.umich.izzy.pkg.${munki_package_name} --install-location / --component-plist Component-${munki_package_name}.plist --version ${version} app.pkg

#Ventura fix
/usr/bin/pkgbuild --root build-root/ --identifier edu.umich.izzy.pkg.${munki_package_name}  --install-location / --component-plist Component-${munki_package_name}.plist --version ${version} app.pkg


#clean up the component file
rm -rf Component-${munki_package_name}.plist

## Create pkg's
#/usr/bin/pkgbuild --root build-root/ --identifier edu.umich.izzy.pkg.$munki_package_name --install-location / --version $version app.pkg

## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-root -name '*.app' -or -name '*.plugin' -or -name '*.prefPane' -or -name '*component' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
#echo /usr/local/munki/makepkginfo -m go-w -g admin -o root app.pkg -f "build-root/Library/Managed\ Installs/aladin"${version}" "${key_files}"" --preinstall_script="preinstall.sh" | /bin/bash > app.plist
echo /usr/local/munki/makepkginfo -m go-w -g admin -o root app.pkg --preinstall_script="preinstall.sh" --postinstall_script="postinstall.sh" -f "build-root/Library/Managed\ Installs/aladin"${version}" "${key_files}"" | /bin/bash > app.plist



## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-root//' app.plist
## END EXAMPLE

# Build pkginfo
#/usr/local/munki/makepkginfo app.pkg > app.plist

plist=`pwd`/app.plist

# Obtain version info
#minver=`/usr/libexec/PlistBuddy -c "Print :installs:0:minosversion" "${plist}"`

# For silent packages, configure blocking_applications and unattended_install
# defaults write "${plist}" blocking_applications -array "VirtualBox.app" "/Path/To/Executable" "iTunesHelper"
# defaults write "${plist}" unattended_install -bool YES

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "10.13.0"
#defaults write "${plist}" minimum_os_version "${minver}"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" icon_name "${icon_name}"
defaults write "${plist}" requires -array "Corretto8"
#defaults write "${plist}" version "${version}"
defaults write "${plist}" unattended_install -bool TRUE


# Obtain display name from Izzy and add to plist
display_name=`/usr/local/bin/displaynametool "${munki_package_name}"`
defaults write "${plist}" display_name -string "${display_name}"

# Obtain description from Izzy and add to plist
description=`/usr/local/bin/descriptiontool "${munki_package_name}"`
defaults write "${plist}" description -string "${description}"

# Obtain category from Izzy and add to plist
category=`/usr/local/bin/cattool "${munki_package_name}"`
defaults write "${plist}" category "${category}"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.pkg   ${prefix}-${version}${suffix}.pkg
mv app.plist ${prefix}-${version}${suffix}.plist

# Notify trello
#ruby $HOME/jenkins-trello/trello-notify.rb "In Testing" "${display_name}" "${version}" "${prefix}-${version}${suffix}.plist" $*
