#!/bin/bash

NEWLOC=`curl -L -k "https://snootles.dsc.umich.edu/packages/aladin" -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' 2>/dev/null | grep '.dmg' | tail -1 | awk -F'"' '{print $2}' |  tr -d '\r'`

if [ "xhttps://snootles.dsc.umich.edu/packages/aladin/${NEWLOC}" != "x" ]; then
	echo "https://snootles.dsc.umich.edu/packages/aladin/${NEWLOC}"
fi
